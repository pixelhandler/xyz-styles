/*jshint node:true*/
/* global require, module */
var EmberAddon = require('ember-cli/lib/broccoli/ember-addon');
var autoprefixer = require('autoprefixer');
var cssnext = require('postcss-cssnext');
var cssimport = require('postcss-import');
var Funnel = require('broccoli-funnel');
var compileCSS = require('broccoli-postcss');
var path = require('path');

module.exports = function(defaults) {
  var options = {
    plugins: [
      { module: autoprefixer, options: { browsers: ['last 2 version'] } },
      { module: cssimport },
      { module: cssnext, options: { sourcemap: true } }
    ]
  };

  var app = new EmberAddon(defaults, { postcssOptions: options });

  var file = 'xyz-styles.css';
  var styles = path.join('./', 'app', 'styles');
  styles = new Funnel(styles, { include: [/.css$/] });
  var css = compileCSS([styles], file, 'vendor/' + file, options.plugins, { inline: false });

  return app.toTree([css]);
};