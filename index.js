/* jshint node: true */
/* global require, module */
'use strict';

module.exports = {
  name: 'xyz-styles',

  getCssFileName: function () {
    return this.name + '.css';
  },

  isAddon: function () {
    var keywords = this.project.pkg.keywords;
    return (keywords && keywords.indexOf('ember-addon') !== -1);
  },

  included: function (app) {
    this._super.included(app);
    if (!this.isAddon()) {
      app.import('vendor/' + this.getCssFileName());
    }
  },

  treeForVendor: function (node) {
    if (this.isAddon()) { return node; }

    var path = require('path');
    var Funnel = require('broccoli-funnel');
    var mergeTrees = require('broccoli-merge-trees');
    var compileCSS = require('broccoli-postcss');

    var styles = path.join(this.project.nodeModulesPath, this.name, 'app', 'styles');
    var inputTrees = new Funnel(styles, { include: [/.css$/] });
    var inputFile = this.getCssFileName();
    var outputFile = inputFile;
    var plugins = this.getPlugins();
    var sourceMaps = { inline: true };
    var css = compileCSS([inputTrees], inputFile, outputFile, plugins, sourceMaps);
    node = (node) ? mergeTrees([ node, css ]) : css;

    return node;
  },

  getPlugins() {
    var autoprefixer = require('autoprefixer');
    var cssnext = require('postcss-cssnext');
    var cssimport = require('postcss-import');

    return [
      { module: autoprefixer, options: { browsers: ['last 2 version'] } },
      { module: cssimport },
      { module: cssnext, options: { sourcemap: true } }
    ];
  }
};
