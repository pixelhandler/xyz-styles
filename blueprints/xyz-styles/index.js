/* jshint node: true */
/* global module */
module.exports = {
  description: 'xyz-styles',

  normalizeEntityName: function () {},

  afterInstall: function () {
    return this.addPackagesToProject([
      { name: 'autoprefixer', target: '^6.3.3' },
      { name: 'broccoli-funnel', target: '^1.0.1' },
      { name: 'broccoli-merge-trees',target: '^1.1.1' },
      { name: 'broccoli-postcss', target: '^2.1.1' },
      { name: 'postcss-cssnext', target: '^2.4.0' },
      { name: 'postcss-import', target: '^8.0.2' }
    ]);
  }
};
