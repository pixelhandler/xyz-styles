# xyz-styles

Styles use PostCSS, this addon is for common styles shared between apps.

## Installing the addon into an app

    ember install git+ssh://git@gitlab.com:pixelhandler/xyz-styles.git

## Development

The stylesheets are in the `app/styles` directory.

* [PostCSS](http://postcss.org)

### Setup the repo

* `git clone` this repository
* `npm install`
* `bower install`

### Linking locally

Use `npm link` in the root of this repo to register the package locally.

Then in your app use `npm install core-styles`.

Run the blueprint with `ember generate core-styles`.

For more information on using ember-cli, visit [http://www.ember-cli.com/](http://www.ember-cli.com/).